import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ShoppingRoutingModule } from './shopping-routing.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { DashboardComponent } from './pages/dashboard/dashboard.component';
import { ProductsComponent } from './pages/products/products.component';
import { ProductComponent } from './pages/product/product.component';
import { CartComponent } from './pages/cart/cart.component';
import { BannerComponent } from './../../layouts/main-layout/utils/banner/banner.component';
import { LastOffersComponent } from './pages/last-offers/last-offers.component';
import { SearchFiltersComponent } from './pages/search-filters/search-filters.component';

@NgModule({
  declarations: [
    DashboardComponent,
    ProductsComponent,
    ProductComponent,
    CartComponent,
    BannerComponent,
    LastOffersComponent,
    SearchFiltersComponent
  ],
  imports: [
    CommonModule,
    ShoppingRoutingModule,
    FormsModule,
    ReactiveFormsModule
  ],
  providers: [],
})
export class ShoppingModule {}
