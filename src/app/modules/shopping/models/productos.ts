export class Productos {
  id: number;
  descripcion: string;
  valorNeto: number;
  iva: number;
  stock: number;
  fechaCreacion: Date;
  fechaActualizacion: Date;
  valorEnvio: number;
  estado: ProductoEstado;
  categoria: Categoria;
  tamaño: Tamaño;
  talla: Categoria;
  color: Categoria;
  vendedor: Vendedor;
  imagenesProduct: ImagenesProduct[];
}

export class Categoria {
  id: number;
  nombre: string;
  descripcion: string;
}

export class ProductoEstado {
  id: number;
  descricion: string;
}

export class ImagenesProduct {
  id: number;
  url: string;
  extension: string;
  principal: boolean;
}

export class Tamaño {
  id: number;
  alto: number;
  ancho: number;
  largo: number;
  grueso: number;
}

export class Vendedor {
  id: number;
  nombre: string;
  tipoDocumento: string;
  numeroDocumento: string;
  direcion: string;
  telefono: string;
  estado: VendedorEstado;
}

export class VendedorEstado {
  id: number;
  descripcion: string;
}
