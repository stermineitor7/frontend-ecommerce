import { Component, OnInit } from '@angular/core';
import * as $ from 'jquery';
import { ActivatedRoute } from '@angular/router';
import { ProductsService } from '../../services/products.service';
import { Productos } from '../../models/productos';

@Component({
  selector: 'app-product',
  templateUrl: './product.component.html',
  styleUrls: ['./product.component.scss'],
})
export class ProductComponent implements OnInit {
  paramsPath = new Map<string, string>();
  viewProduct: boolean = false;
  product: Productos;

  constructor(
    public _sProducts: ProductsService,
    private activatedRouter: ActivatedRoute
  ) {
    this.validateParmsMap();
    this.getProducts('1');
  }

  ngOnInit() {}

  validateParmsMap() {
    this.viewProduct =
      this.activatedRouter.snapshot.paramMap.has('product') &&
      this.activatedRouter.snapshot.paramMap.has('description');
    if (this.viewProduct) {
      this.paramsPath.set(
        'product',
        this.activatedRouter.snapshot.params.product
      );
      this.paramsPath.set(
        'description',
        this.activatedRouter.snapshot.params.description
      );
      this.getProducts(this.paramsPath.get('product'));
    }
  }

  getProducts(productId: string) {
    this._sProducts.getProduct(productId).subscribe(
      (response) => {
        this.product = response;
      },
      (error) => {
        console.info('Error: ' + error);
      }
    );
  }
}
