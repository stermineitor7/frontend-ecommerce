import { Component, OnInit } from '@angular/core';
import { ProductsService } from 'src/app/modules/shopping/services/products.service';
import { Productos } from 'src/app/modules/shopping/models/productos';

@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.scss'],
})
export class ProductsComponent implements OnInit {
  listProducts: Productos[] = [];

  constructor(public _sProducts: ProductsService) {}

  ngOnInit(): void {
    this.getProducts();
  }

  getProducts() {
    this._sProducts.getProducts().subscribe(
      (response) => {
        this.listProducts = response;
      },
      (error) => {
        console.info('Error: ' + error);
      }
    );
  }
}
