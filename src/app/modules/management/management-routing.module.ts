import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { SellingProductsComponent } from './pages/selling-products/selling-products.component';
import { SellingProductComponent } from './pages/selling-product/selling-product.component';
import { AddProductComponent } from './pages/add-product/add-product.component';
import { UpdateProductComponent } from './pages/update-product/update-product.component';

const routes: Routes = [
  {
    path: '',
    children: [
      {
        path: '',
        redirectTo: 'productos',
        pathMatch: 'prefix',
      },
      {
        path: 'productos',
        component: SellingProductsComponent,
      },
      {
        path: 'productos/:product',
        component: SellingProductComponent,
      },
      {
        path: 'agregar/productos',
        component: AddProductComponent,
      },
      {
        path: 'editar/productos/:product',
        component: UpdateProductComponent,
      },
    ],
  },
];

@NgModule({
  declarations: [],
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ManagementRoutingModule {}
