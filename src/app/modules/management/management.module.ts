import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ManagementRoutingModule } from './management-routing.module';
import { SellingProductsComponent } from './pages/selling-products/selling-products.component';
import { SellingProductComponent } from './pages/selling-product/selling-product.component';
import { AddProductComponent } from './pages/add-product/add-product.component';
import { UpdateProductComponent } from './pages/update-product/update-product.component';

@NgModule({
  declarations: [SellingProductsComponent, SellingProductComponent, AddProductComponent, UpdateProductComponent],
  imports: [
    CommonModule,
    ManagementRoutingModule,
    FormsModule,
    ReactiveFormsModule,
  ],
  providers: [],
})
export class ManagementModule {}
