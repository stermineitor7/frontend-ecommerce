export class Footer {
  idFooter: number;
  activo: boolean;
  fechaCreacion: Date;
  imagen: string;
  link: string;
  nombre: string;
  posicion: string;
}
