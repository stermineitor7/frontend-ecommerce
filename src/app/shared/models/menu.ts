export class Menu {
  idMenu: number;
  imagen: string;
  link: string;
  nombre: string;
  orden: number;
  idPadre: number;
  fechaCreacion: Date;
  activo: boolean;
  posicion: string;
  titulo: string;
  menus: Menu[];
}
