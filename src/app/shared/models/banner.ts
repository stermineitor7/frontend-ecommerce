export class Banner {
  idBanner: number;
  descripcion: string;
  activo: boolean;
  fechaCreacion: Date;
  imagenPrincipal: string;
  imagenResponsive: string;
  link: string;
  nombreBoton: string;
  posicion: string;
  titulo: string;
}
