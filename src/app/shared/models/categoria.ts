import { Footer } from './footer';

export class Categoria {
  idCategoria: number;
  nombre: string;
  activo: boolean;
  fechaCreacion: Date;
  posicion: number;
  categoriaFooter: Footer[];
}
