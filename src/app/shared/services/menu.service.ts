import { Injectable } from '@angular/core';
import { Observable, throwError } from 'rxjs';
import { map, catchError, retry } from 'rxjs/operators';
import { environment } from '../../../environments/environment';

import {
  HttpHeaders,
  HttpClient,
  HttpParams,
  HttpErrorResponse,
} from '@angular/common/http';
import { CONSTANTS } from './../constants/constants';

@Injectable({
  providedIn: 'root',
})
export class MenuService {
  private url: string;
  private headers = new HttpHeaders();

  constructor(private _http: HttpClient) {
    this.conectionConfig();
    this.headers.append('Cache-control', 'no-cache');
    this.headers.append('Cache-control', 'no-store');
    this.headers.append('Expires', '0');
    this.headers.append('Pragma', 'no-cache');
  }

  //Config conection services
  private conectionConfig(): void {
    this.url = environment.apiMenuService;
  }

  public getMenu() {
    let params = new HttpParams();
    return this._http
      .get<any>(this.url + 'menu', {
        headers: this.headers,
        params: params,
      })
      .pipe(
        map((res) => res),
        retry(1), // Se repite la peticion si no se tiene una repuesta satisfactoria, debido a la respuesta del cliente
        catchError(this.handleError)
      );
  }

  public getMenuActive() {
    let params = new HttpParams();
    return this._http
      .get<any>(this.url + 'menu/active', {
        headers: this.headers,
        params: params,
      })
      .pipe(
        map((res) => res),
        retry(1), // Se repite la peticion si no se tiene una repuesta satisfactoria, debido a la respuesta del cliente
        catchError(this.handleError)
      );
  }

  public addMenu(data: any) {
    let params = new HttpParams();
    return this._http
      .post<any>(this.url + 'url/', data, {
        headers: this.headers,
      })
      .pipe(
        map((res) => res),
        retry(1), // Se repite la peticion si no se tiene una repuesta satisfactoria, debido a la respuesta del cliente
        catchError(this.handleError)
      );
  }

  public updateMenu(data: any) {
    let params = new HttpParams();
    return this._http
      .put<any>(this.url + 'url/', data, { headers: this.headers })
      .pipe(
        map((res) => res),
        retry(1), // Se repite la peticion si no se tiene una repuesta satisfactoria, debido a la respuesta del cliente
        catchError(this.handleError)
      );
  }

  /**
   * Manejo de errores en los servicios.
   * @param {HttpErrorResponse} error.
   */
  private handleError(error: HttpErrorResponse) {
    if (error.error instanceof ErrorEvent) {
      // Se produjo un error en el lado del cliente o en la red.
      console.error(CONSTANTS.msgErrorEventService, error.error.message);
    } else {
      // El servidor devolvió un código de respuesta fallido.
      console.error(
        CONSTANTS.msgErrorCodeService + `(${error.status}) ${error.message}`
      );
    }
    // Mensaje generico para el usuario
    return throwError(CONSTANTS.msgErrorGenericService);
  }
}
