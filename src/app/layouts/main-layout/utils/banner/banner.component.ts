import { Component, OnInit } from '@angular/core';
import { Banner } from 'src/app/shared/models/banner';
import { BannerService } from 'src/app/shared/services/banner.service';

@Component({
  selector: 'app-banner',
  templateUrl: './banner.component.html',
  styleUrls: ['./banner.component.scss'],
})
export class BannerComponent implements OnInit {
  listBanner: Banner;

  constructor(private _sBanner: BannerService) {
    this.getMenuActive();
  }

  ngOnInit(): void {}

  getMenuActive() {
    this._sBanner.getBannerActive().subscribe(
      (response) => {
        this.listBanner = response;
      },
      (error) => {
        console.info('Error: ' + error);
      }
    );
  }
}
