import { Component, OnInit } from '@angular/core';
import { Menu } from 'src/app/shared/models/menu';
import { MenuService } from 'src/app/shared/services/menu.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss'],
})
export class HeaderComponent implements OnInit {
  listMenu: Menu;

  constructor(private _sMenu: MenuService) {
    this.getMenuActive();
  }

  ngOnInit() {}

  getMenuActive() {
    this._sMenu.getMenuActive().subscribe(
      (response) => {
        this.listMenu = response;
      },
      (error) => {
        console.info('Error: ' + error);
      }
    );
  }
}
