import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-main-layout',
  templateUrl: './main-layout.component.html',
  styleUrls: ['./main-layout.component.scss'],
})
export class MainLayoutComponent implements OnInit {
  routerNavigate: string;

  constructor(private router: Router) {
    this.routerNavigate = this.router.url;
  }

  ngOnInit() {}

  ngOnChange() {
    this.routerNavigate = this.router.url;
  }
}
