import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VoidLayoutComponent } from './void-layout.component';

describe('VoidLayoutComponent', () => {
  let component: VoidLayoutComponent;
  let fixture: ComponentFixture<VoidLayoutComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VoidLayoutComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VoidLayoutComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
